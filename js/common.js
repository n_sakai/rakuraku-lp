$(function(){
    var ua = navigator.userAgent;
    if((ua.indexOf('iPhone') > 0) || ua.indexOf('iPod') > 0 || (ua.indexOf('Android') > 0 && ua.indexOf('Mobile') > 0)){
        $('head').prepend('<meta name="viewport" content="width=device-width,initial-scale=1">');
    } else {
        $('head').prepend('<meta name="viewport" content="width=1200">');
    }
});



//スムーススクロール
$(function(){
	$('a[href^="#"]').click(function(){
		var href= $(this).attr("href");
		var target = $(href == "#" || href == "" ? "body" : href);
		var position = target.offset().top;
		$("html, body").animate({scrollTop:position}, 500, "swing");
		return false;
	});
});



//TOPへ戻るボタン
$(function(){
  var pagetop = $(".js-pagetop");
  $(window).scroll(function () {
    if($(this).scrollTop() >= 300) {
      pagetop.fadeIn();
    } else {
     	pagetop.fadeOut();
    }
  });
});



$(window).on("scroll", function() {
	$('.js-header').css("left", -$(window).scrollLeft());
})